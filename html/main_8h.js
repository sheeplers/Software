var main_8h =
[
    [ "LORA_ENABLE_PIN", "group__Main.html#ga5a0e9d1b822749d2ded3339b72b9403e", null ],
    [ "LORA_ENABLE_PORT", "group__Main.html#ga784b089d9fb112a8fbdabe0cb949803a", null ],
    [ "VBAT_ADC_CHANNEL", "group__Main.html#ga4f20ac24da8561cbc30fdf6c4f24d915", null ],
    [ "VBAT_ADC_PER", "group__Main.html#gad81d6029beb0dd8103e188492a88caad", null ],
    [ "VBAT_MEAS_PIN", "group__Main.html#ga103076d710153b256c3ef32ac89c1b92", null ],
    [ "VBAT_MEAS_PORT", "group__Main.html#ga585bac0d874b7bad6472906f1db22c89", null ],
    [ "VBAT_PIN", "group__Main.html#ga7ddd8f8ba527400d806a6b41eaa95be6", null ],
    [ "VBAT_PORT", "group__Main.html#ga885ee32d9c7043a93ccff3ecae176921", null ],
    [ "adcInit", "group__Main.html#gafd645b17cf0581bcaa22c56011c775e4", null ],
    [ "gpioInit", "group__Main.html#gadab76f1253da89e657056817a3a9a11a", null ],
    [ "measureVBAT", "group__Main.html#gaee8789884801f67bf1a45ee0165aa1e8", null ]
];