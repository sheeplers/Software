var searchData=
[
  ['vbat_5fadc_5fchannel',['VBAT_ADC_CHANNEL',['../group__Main.html#ga4f20ac24da8561cbc30fdf6c4f24d915',1,'main.h']]],
  ['vbat_5fadc_5fper',['VBAT_ADC_PER',['../group__Main.html#gad81d6029beb0dd8103e188492a88caad',1,'main.h']]],
  ['vbat_5fmeas_5fpin',['VBAT_MEAS_PIN',['../group__Main.html#ga103076d710153b256c3ef32ac89c1b92',1,'main.h']]],
  ['vbat_5fmeas_5fport',['VBAT_MEAS_PORT',['../group__Main.html#ga585bac0d874b7bad6472906f1db22c89',1,'main.h']]],
  ['vbat_5fpin',['VBAT_PIN',['../group__Main.html#ga7ddd8f8ba527400d806a6b41eaa95be6',1,'main.h']]],
  ['vbat_5fport',['VBAT_PORT',['../group__Main.html#ga885ee32d9c7043a93ccff3ecae176921',1,'main.h']]]
];
