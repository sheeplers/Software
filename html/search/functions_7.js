var searchData=
[
  ['send_5fcommand',['send_command',['../group__RN2483.html#ga43e4c500d83f18a7222fe2ba4894b5e9',1,'rn2483.c']]],
  ['setup_5fretransmissions',['setup_retransmissions',['../group__RN2483.html#ga725cbfd618403e37e96b0dc91cc97c8c',1,'rn2483.c']]],
  ['spiinit',['SPIInit',['../group__LIS3DH.html#gaa738ada19c5774090d23f39b5beecb2b',1,'LIS3DH.c']]],
  ['spireadbyte',['SPIReadByte',['../group__LIS3DH.html#gac832b8fcf29ce75e9aa64bdcda0619bf',1,'LIS3DH.c']]],
  ['spireaddata',['SPIReadData',['../group__LIS3DH.html#gaadf670a081f62262c456d2867aaa0133',1,'LIS3DH.c']]],
  ['spisendbyte',['SPISendByte',['../group__LIS3DH.html#gabb89ff28140baff3ac2202f1c8a0407f',1,'LIS3DH.c']]],
  ['spisenddata',['SPISendData',['../group__LIS3DH.html#ga5046352ff59f2f873a67b5fbdaeb7954',1,'LIS3DH.c']]],
  ['strcompare',['StrCompare',['../group__STRCONV.html#ga8be5df54b86c039f81d2496db2f375de',1,'StringConversions.c']]],
  ['systick_5fhandler',['SysTick_Handler',['../group__Main.html#gab5e09814056d617c521549e542639b7e',1,'main.c']]]
];
