/**
 * Copyright (c) 2018 Tijl Schepens, Cynthia Blancke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

/**
  ******************************************************************************
  * @file    src/main.c
  * @author  Tijl Schepens
  * @version V1.0.0
  * @date    04-04-2018
  * @brief   Main file.
  *
  *          Where it all comes togheter.
  ******************************************************************************
  */

/** @addtogroup Main
 * @{
 */

#include "main.h"
/* Used for controlling the LIS3DH sensor. */
#include "LIS3DH.h"
/* The LoRa module driver. */
#include "rn2483.h"

/* Global boolean keeps track of the interrupt status. */
bool sendAlert = false;

volatile uint32_t msTicks; /* counts 1ms timeTicks */

/***************************************************************************//**
 * @brief SysTick_Handler
 * Interrupt Service Routine for system tick counter
 ******************************************************************************/
void SysTick_Handler(void)
{
  msTicks++;       /* Increment counter. */
}

int main(void)
 {
  RTCDRV_TimerID_t timer1;
  /* Chip errata */
  CHIP_Init();

  /* Setup SysTick Timer for 1 msec interrupts  */
  if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000)) {
    while (1);
  }

  /* Initialize all the necessary GPIO's. */
  gpioInit();
  /* Initialize the ADC peripheral. */
  adcInit();

  /* Initialize the LIS3DH accelerometer. */
  InitLIS3DH();
  /* Enable interrupts when a certain angle is reached. */
  LIS3DH_Enable_Detection4D();

  /* Initialize the LoRa module. */
  rn2483_init(setupABP);
  /* Peform an Over The Air Activation. Retry 4 times. */
  bool result = false;
  uint8_t tries = 0;
  while(result == false && tries < 4){
    result = rn2483_join_abp();
    tries++;
  }

  if(result == false){
    while(1);
  }

  /* Initialize the RTC driver. */
  RTCDRV_Init();
  RTCDRV_AllocateTimer(&timer1);

  /* Infinite main loop */
  while (1) {
    char payload[16] = { 0 };

    if(sendAlert == true){
      char payload[] = "f9";
      rn2483_send_payload(1, payload);
      sendAlert = false;
    }

    if(GPIO_PinInGet(LIS3DH_INT1_PORT, LIS3DH_INT1_PIN) && sendAlert == false){
      LIS3DH_ReadInt1Src();
    }

    /* Timer in ms. */
    SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
    RTCDRV_StartTimer(timer1, rtcdrvTimerTypeOneshot, 86400000, NULL, NULL);
    /* Go into sleep. */
    EMU_EnterEM2(true);
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;

    /* Only send battery voltage if the device woke up from the RTC and not from
     * an interrupt from the LIS3DH
     */
    if(sendAlert == false){
      uint16_t vBat = measureVBAT();
      sprintf(payload, "%X%X%X%X", (vBat>>12)&0xF, (vBat>>8)&0xF, (vBat>>4)&0xF, vBat&0xF);

      rn2483_send_payload(1, payload);
    }
  }
}

/**
 * @brief Initialization routine for the GPIOs
 */
void gpioInit(void){
  /* Enable the peripheral clock for the GPIO. */
  CMU_ClockEnable(cmuClock_GPIO, true);

  /* Initialize the GPIOs. */
  GPIO_PinModeSet(LORA_ENABLE_PORT, LORA_ENABLE_PIN, gpioModePushPull, 1);
  GPIO_PinModeSet(VBAT_PORT, VBAT_PIN, gpioModeInput, 0);
  GPIO_PinModeSet(VBAT_MEAS_PORT, VBAT_MEAS_PIN, gpioModePushPull, 0);
}

/**
 * @brief Initialization routine for the ADC peripheral
 */
void adcInit(void){
  ADC_Init_TypeDef adcInit;
  ADC_InitSingle_TypeDef adcSingleInit;

  /* Enable the ADC clock. */
  CMU_ClockEnable(cmuClock_ADC0, true);

  /* Initialize the ADC peripheral. */
  adcInit.ovsRateSel = adcOvsRateSel2; // Two samples per conversion
  adcInit.lpfMode = adcLPFilterDeCap; // Enable the decoupling capacitor on the ADC input
  adcInit.warmUpMode = adcWarmupNormal; // Normal warm-up sequence (takes approx 6µs)
  adcInit.timebase = ADC_TimebaseCalc(0); // Calculate a time base of at least 1µs for the ADC warm-up
  adcInit.prescale = ADC_PrescaleCalc(1000000, 0); // Choose a reasonable ADC clock of 1MHz
  adcInit.tailgate = false; // Tailgating is unnecessary as we will not use scan mode
  ADC_Init(VBAT_ADC_PER, &adcInit);

  /* Set up the ADC for single conversions. */
  adcSingleInit.prsSel = adcPRSSELCh0; // The peripheral reflex channel 0 can be used to trigger conversions
  adcSingleInit.acqTime = adcAcqTime16; // ToDo: Choose a sane acquisition time
  adcSingleInit.reference = adcRef2V5; // Use the 2,5V internal reference. WARNING: Do not exceed 2,5V
  adcSingleInit.resolution = adcRes12Bit; // Maximum resolution
  adcSingleInit.input = VBAT_ADC_CHANNEL; // Single ended input, channel 4 (ADC0_CH4)
  adcSingleInit.diff = false; // Single ended input is used for battery voltage measurements
  adcSingleInit.prsEnable = false; // Feature is unused for now
  adcSingleInit.leftAdjust = false; // No left adjustment needed
  adcSingleInit.rep = false; // Continuous conversion is not used
  ADC_InitSingle(VBAT_ADC_PER, &adcSingleInit);
}

/**
 * @brief Measure the battery voltage.
 * To calculate the battery voltage:
 * (2,5V/4096)*METING = Battery voltage
 *
 * @return Battery voltage in 16-bit
 */
uint16_t measureVBAT(){
  /* Enable the voltage divider. */
  GPIO_PinOutClear(VBAT_MEAS_PORT, VBAT_MEAS_PIN);
  /* Do a single ADC conversion. */
  ADC_Start(VBAT_ADC_PER, adcStartSingle);
  while(!(VBAT_ADC_PER->STATUS & (1 << 16))); // Wait for the conversion to complete
  /* Disable the voltage divider. */
  GPIO_PinOutSet(VBAT_MEAS_PORT, VBAT_MEAS_PIN);
  return (uint16_t)ADC_DataSingleGet(VBAT_ADC_PER); // Get the conversion result
}

/** @}*/
