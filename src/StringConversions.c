/**
 * Copyright (c) 2018 Tijl Schepens
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

/**
  ******************************************************************************
  * @file    src/StringConversions.c
  * @author  Tijl Schepens
  * @version V1.0.0
  * @date    22-09-2017
  * @brief   Functions used for string conversions.
  *
  *          This file contains all the functions
  *          that are used to manipulate strings
  ******************************************************************************
  */

/** @addtogroup STRCONV String Conversions
 * @{
 */

#include "StringConversions.h"

/**
 * @brief Compare two strings
 *
 * @param str1    First string to compare
 * @param str2    Second string to compare
 * @param cmpLen  The length of both strings to compare
 *
 * @retval  1: Strings are the same
 * @retval  0: Strings are different
 */
int StrCompare(char *str1, const char *str2, int cmpLen)
{
  for(int i=0;i<cmpLen;i++){
    if(str1[i] != str2[i])
      return 0;
  }
  /* Will only reach here if the strings are the same. */
  return 1;
}

/** @}*/
