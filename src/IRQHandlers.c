/**
 * Copyright (c) 2018 Tijl Schepens, Cynthia Blancke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

/**
  ******************************************************************************
  * @file    src/IRQHandlers.c
  * @author  Tijl Schepens
  * @version V1.0.0
  * @date    04-04-2018
  * @brief   IRQHandlers file.
  *
  *          This file contains all the functions
  *          to handle interrupts.
  ******************************************************************************
  */

/** @addtogroup IRQHandlers
 * @{
 */

#include "IRQHandlers.h"
#include "LIS3DH.h"

/**
 * @brief Interrupt handler for even GPIO pins.
 */
void GPIO_EVEN_IRQHandler(void)
{
  if(GPIO_PinInGet(LIS3DH_INT1_PORT, LIS3DH_INT1_PIN)){
    GPIO_IntClear(1<<LIS3DH_INT1_PIN);
    sendAlert = true;
    LIS3DH_ReadInt1Src();
  }
}

/**
 * @brief Interrupt handler for odd GPIO pins.
 */
void GPIO_ODD_IRQHandler(void)
{
  if(GPIO_PinInGet(LIS3DH_INT2_PORT, LIS3DH_INT2_PIN)){
    GPIO_IntClear(1<<LIS3DH_INT2_PIN);
    LIS3DH_ReadInt1Src();
  }
}

/**
 * @brief LEUART0 interrupt handler
 * Handles send/receive and error interrupts.
 */
void LEUART0_IRQHandler(void)
{
  /* Received data is valid. */
  if(LEUART_IntGet(RN2483_LEUART) & LEUART_IF_RXDATAV){
    /* Put received data in buffer. */
    LoRaRxBuffer[LoRaRxBufferIndex++] = LEUART_RxDataGet(RN2483_LEUART);

    /* Acknowledge interrupt. */
    LEUART_IntClear(LEUART0, LEUART_IF_RXDATAV);
  }
  /* Receive overflow occurred. Just acknowledge the interrupt. */
  else if(LEUART_IntGet(RN2483_LEUART) & LEUART_IF_RXOF){
    /* Acknowledge interrupt. */
    LEUART_IntClear(LEUART0, LEUART_IF_RXOF);
  }
  /* Receive underflow occurred. Just acknowledge the interrupt. */
  else if(LEUART_IntGet(RN2483_LEUART) & LEUART_IF_RXUF){
    /* Acknowledge interrupt. */
    LEUART_IntClear(LEUART0, LEUART_IF_RXUF);
  }
}

/** @}*/
