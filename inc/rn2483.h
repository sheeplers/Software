/* Licensing needs to be discussed. */

/**
  ******************************************************************************
  * @file    inc/main.h
  * @author  Jaimy Declercq, Tijl Schepens
  * @version V1.2.0
  * @date    06-03-2018
  * @brief   RN2483 library header file.
  *
  *          File containts all function prototypes, enums,
  *          defines, inclusions,...
  ******************************************************************************
  */

#ifndef __RN2483_H_
#define __RN2483_H_

/** @addtogroup RN2483
 * @{
 */

#include "main.h"
#include "stdio.h"
#include "StringConversions.h"

/* Global defines. */
#define MAX_JOIN_REQUESTS 5
#define LORA_RX_BUFFER_LENGTH 100   /**< Length of the UART buffer for the LoRa module. */

/* Pins used for the LoRa module. */
/** LEUART Tx port. */
#define RN2483_LEUART_Tx_Port   gpioPortB   /**< GPIOB */
/** LEUART Tx pin. */
#define RN2483_LEUART_Tx_Pin    13          /**< PB13  */
/** LEUART Rx port. */
#define RN2483_LEUART_Rx_Port   gpioPortB   /**< GPIOB */
/** LEUART Rx pin. */
#define RN2483_LEUART_Rx_Pin    14          /**< PB14  */

/* LEUART settings. */
/** LEUART peripheral used. (0, 1, ...) */
#define RN2483_LEUART           LEUART0           /**< LEUART0 */
/** LEUART clock that needs to be enabled. */
#define RN2483_LEUART_CLOCK     cmuClock_LEUART0  /**< LEUART0 clock */
/** The output of the LEUART peripheral can be mapped to different
 *  pins. The different pins are marked with a location
 *  between 0 and 6.
 */
#define RN2483_LEUART_PINS_LOC (1 << 8)

/** A timeout for send and receive functions (in ms). */
#define LORA_TIMEOUT           10000    /**< An optimum has not been found here yet. Saving for example takes 1.15s. */

/** Enumeration with messages used by wait_response_complete. */
typedef enum
{
  ReceiveTimeout,             /**< A timeout occurred while waiting for a response. */
  ReceiveOk,                  /**< Receiving a response from the LoRa module was successful. */
  ReceiveError,               /**< An error occurred while receiving the response. */
  ReceivedAccepted,           /**< The join procedure was successful. */
  ReceivedBusy,               /**< The MAC is not in idle state. */
  ReceivedDenied,             /**< The module attempted to join the network but was rejected. */
  ReceivedFrameCounterError,  /**< The frame counter rolled over. */
  ReceivedInvalidDataLength,  /**< Payload length is greater than the max application payload
                                   length corresponding to the current data rate. */
  ReceivedInvalidParameter,   /**< Invalid parameters were send to the module. */
  ReceivedMACRxAC,            /**< Transmission was successful on port 1 and got response AC. */
  ReceivedMACRxAF,            /**< Transmission was successful on port 1 and got response AF. */
  ReceivedMACTxOK,            /**< Uplink to the server was successful with no downlink response. */
  ReceivedMACError,           /**< Transmission was unsuccessful. No ACK was received from the server. */
  ReceivedNotJoined,          /**< The module hasn't joined the network jet! */
  ReceivedNoFreeCh,           /**< All channels are currently busy. */
  ReceivedOK,                 /**< Received command and parameters are valid. */
  ReceivedSilent,             /**< The module is in a silent immediately state. */
  ReceivedMACPause,           /**< MAC is paused and wasn't resumed again. */
  ReceivedVersion,            /**< Got the module firmware version. */
  ReceivedNULL                /**< Response was NULL. */
} ReceiveResponse;

/** Enumeration used in the initialization procedure. */
typedef enum
{
  setupABP,                   /**< Set up the RN2483 to use ABP. */
  setupOTAA                   /**< Set up the RN2483 to use OTAA. */
} JoinSetup;

/** Enumeration used for return values. */
typedef enum
{
  RN2483_OK,                  /**< The function returned without encountering any problems. */
  RN2483_JoinOK,              /**< The join succeeded. */
  RN2483_JoinFailed,          /**< The join failed. */
  RN2483_Error                /**< A problem was encountered while executing the function. */
} RN2483_Status;

/* High level functions to be used by the user. */
void rn2483_init(JoinSetup setupKeys);

RN2483_Status rn2483_set_deveui();
uint8_t rn2483_set_appeui();
uint8_t rn2483_set_appkey();
uint8_t rn2483_set_power_max();
uint8_t rn2483_set_devaddr();
uint8_t rn2483_set_nwskey();
uint8_t rn2483_set_appskey();
uint8_t rn2483_sys_reset();
uint8_t rn2483_set_power_idx();

void rn2483_get_mac();
uint8_t rn2483_get_ver();

RN2483_Status rn2483_join_otaa();
RN2483_Status rn2483_join_abp();

uint8_t rn2483_enable_adr();
uint8_t rn2483_save_mac();
bool rn2483_send_payload(uint8_t portNo, char payload[]);

/* Low level functions. */
void LEUARTInit();
uint8_t setup_duty_cycle();
uint8_t setup_channels();
uint8_t setup_retransmissions(int Retransmissions);
ReceiveResponse send_command(const char * command);
ReceiveResponse ParseResponse(uint16_t timeout);

/** @}*/

#endif /* SRC_RN2483_H_ */
