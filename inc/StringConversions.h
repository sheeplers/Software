/**
 * Copyright (c) 2018 Tijl Schepens
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

/**
  ******************************************************************************
  * @file    inc/StringConversions.h
  * @author  Tijl Schepens
  * @version V1.0.0
  * @date    22-09-2017
  * @brief   String conversions header file.
  *
  *          This file contains all the string
  *          conversion function headers.
  ******************************************************************************
  */

#ifndef STRING_CONVERSIONS_H_
#define STRING_CONVERSIONS_H_

/** @addtogroup STRCONV String Conversions
 * @{
 */

/* Public functions. */
int StrCompare(char *str1, const char *str2, int cmpLen);

/** @}*/

#endif
