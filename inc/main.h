/**
 * Copyright (c) 2018 Tijl Schepens, Cynthia Blancke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

/**
  ******************************************************************************
  * @file    inc/main.h
  * @author  Tijl Schepens, Cynthia Blancke
  * @version V1.0.0
  * @date    31-03-2018
  * @brief   main header file.
  *
  *          This file contains all the pin definitions,
  *          enums, structs, includes,...
  *
  *          NOTE: Watch out for recursive inclusions
  ******************************************************************************
  */

#ifndef __MAIN_H
#define __MAIN_H

/** @addtogroup Main
 * @{
 */

#include "em_leuart.h"

#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
/* For measuring the battery voltage. */
#include "em_adc.h"
#include "em_rtc.h"
#include "em_emu.h"
#include "rtcdriver.h"

/* Global defines. */
/** Battery voltage port. */
#define VBAT_PORT         gpioPortD /**< Port D */
/** Battery voltage pin. */
#define VBAT_PIN          5         /**< PD4 */
/** ADC peripheral used to measure the battery voltage. */
#define VBAT_ADC_PER      ADC0      /**< ADC0 */
/** ADC channel used to measure the battery voltage. */
#define VBAT_ADC_CHANNEL  4         /**< ADC0_CH4 */
/** Enable battery voltage port. */
#define VBAT_MEAS_PORT    gpioPortD /**< Port D */
/** Enable battery voltage pin. */
#define VBAT_MEAS_PIN     5         /**< PD5 */

/** Enable LoRa module port. */
#define LORA_ENABLE_PORT  gpioPortA /**< Port A */
/** Enable LoRa module pin. */
#define LORA_ENABLE_PIN   8         /**< PA8 */

/* Queue and buffer sizes. */

/* Structs and enums. */

/* Main function prototypes. */
void gpioInit(void);
void adcInit(void);
uint16_t measureVBAT();

/** @}*/

#endif /* __MAIN_H */
